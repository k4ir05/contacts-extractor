require 'sqlite3'
require 'vpim/vcard'
require 'shoes'

Shoes.app(title: 'Contacts converter') {
	button 'Convert' do
		filename = ask_open_file
		# SQLite3::Database.new("contacts.sqlite", results_as_hash: true) do |db|
		SQLite3::Database.new(filename, results_as_hash: true) do |db|
		  file = File.open 'contacts.vcf', 'w'
		  # rows = db.execute("SELECT * FROM ZWACONTACT")
		  db.execute("SELECT * FROM ZWACONTACT") do |row|
		    card = Vpim::Vcard::Maker.make2 do |maker|
			  id = row['Z_PK'].to_i
			  firstname = row['ZFIRSTNAME']
			  indexname = row['ZINDEXNAME']
			  fullname = row['ZFULLNAME']
			  puts "firstname: #{firstname}, fullname: #{fullname}"
			  maker.add_name do |name|
			  	name.family = firstname unless firstname.nil?
			  	name.fullname = fullname unless fullname.nil?
			  end
			  # puts sql_str = "SELECT * FROM ZWAPHONE WHERE ZCONTACT = #{id}"
			  # phones = db.execute sql_str
			  # puts "phones_count: #{phones.length}"
			  # phones.each do |phone_row|
			  db.execute("SELECT * FROM ZWAPHONE WHERE ZCONTACT = #{id}") do |phone_row|
			    label = phone_row['ZLABEL']
			    phone = phone_row['ZPHONE']
			    status = phone_row['ZSTATUS']
			    favorite = phone_row['ZFAVORITE']
			    whatsappid = phone_row['ZWHATSAPPID']
			    puts "phone: #{phone}"
			    maker.add_tel phone unless phone.nil?
			  end
			end
			puts '-'*50
			file << card
		  end
		  alert 'Done!'
		end
	end
}
